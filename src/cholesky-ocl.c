/* 
  This program contains code executing the Cholesky Decomposition on GPU, using OpenCL.
  This program can be used for benchmarking.
  Input:
    <size of matrix> <algorithm> <cutoff> 
      <optional: platform id (default 0)> <optional: device id (default 0)> <optional: compare results (default 0)>
    where:
      size of matrix: size of a single dimension on the input matrix
      algorithm: 
        0: sequential (for reference)
        1: simple single kernel
        2: simple multiple kernel
        3: in-place multiple kernel
      cutoff: how many columns will be executed on CPU, sequentially

  The program will attempt to execute on the given platform and device, and print on standard output:
    <size of matrix>,<algorithm>,<cutoff>,<ellapsed host time>

  Written by João Paulo T Ruschel, 2016
    Contact: jptruschel@inf.ufrgs.br

  Special thanks to Anteru for providing most of the OpenCL initialization code
    https://bitbucket.org/Anteru/opencltutorial
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "hpcelo.h"
#include "spd_matrix.h"

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

// Max OpenCL Platforms and Devices
#define MAX_OCL_PLATFORMS 8
#define MAX_OCL_DEVICES 8

// Default values (if none specified)
#define DEFAULT_MATRIX_SIZE 1000
#define DEFAULT_CUTOFF 64

// Max OpenCL source file (.cl)
#define MAX_SOURCE_SIZE (0x100000)

// Error
#define EPSILON 0.001

// Sequential implementation of Cholesky-Crout Algorithm
int cholesky_sequential(float *A, float *L, unsigned int dimensionSize) {
  if (A == NULL || L == NULL)
    return 1;

  float sum, value, diagonal_value;
  int i, j, k;

  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[j * dimensionSize + k] * L[j * dimensionSize + k];
    }
    L[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i * dimensionSize + k] * L[j * dimensionSize + k];
      }
      L[i * dimensionSize + j] = (1.0 / L[j * dimensionSize + j] * 
        (A[i * dimensionSize + j] - sum));
    }
  }

  return 0;
}

// Sequential implementation of Cholesky-Crout Algorithm
int cholesky_sequential_timed(float *A, float *L, unsigned int dimensionSize) {
  if (A == NULL || L == NULL)
    return 1;

  float sum, value, diagonal_value;
  int i, j, k;

  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;
  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[j * dimensionSize + k] * L[j * dimensionSize + k];
    }
    L[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i * dimensionSize + k] * L[j * dimensionSize + k];
      }
      L[i * dimensionSize + j] = (1.0 / L[j * dimensionSize + j] * 
        (A[i * dimensionSize + j] - sum));
    }
  }
  HPCELO_END_TIMER;

  printf("0,%0.8f\n", HPCELO_GET_TIMER);  // 0 GPU time, CPU time

  return 0;
}

// Checks if the given cl_int is an error state, exiting the program with an error message.
void CheckError(cl_int error)
{
  if (error != CL_SUCCESS) {
    fprintf(stderr, "OpenCL call failed with error %d\n", error);
    exit(1);
  }
}
// Loads a CL file with kernels
//  Returns the source's size, and a pointer to the code on source_str
size_t LoadKernel(const char* name, char** source_str)
{
  FILE *fp;
  size_t source_size;

  fp = fopen(name, "r");
  if (!fp) {
    fprintf(stderr, "Failed to load kernel.\n");
    exit(1);
  }
  *source_str = (char*)malloc(MAX_SOURCE_SIZE);
  source_size = fread(*source_str, 1, MAX_SOURCE_SIZE, fp);
  fclose(fp);

  return source_size;
}
// Creates an OpenCL program from the given source code
cl_program CreateProgram(const char* source_str, size_t source_size,
  cl_context context)
{
  // http://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clCreateProgramWithSource.html
  cl_int error;
  cl_program program = clCreateProgramWithSource(context, 1,
    (const char **)&source_str, (const size_t *)&source_size, &error);
  CheckError(error);

  return program;
}

cl_program CreateProgramFromKernel(const char* name, cl_context context) {
  size_t source_size;
  char* source_str;
  source_size = LoadKernel(name, &source_str);
  return CreateProgram(source_str, source_size, context);
}

void cholesky_gpu_simple_single(cl_program program, cl_command_queue queue, cl_mem aBuffer, cl_mem lBuffer, unsigned int dimensionSize, 
  float *A, float *L, unsigned int cutoff) {
  cl_int error;

  // Build kernel
  cl_kernel kernel_column = clCreateKernel(program, "choleskyColumnSimple", &error);
  CheckError(error);

  // Set Arguments
  clSetKernelArg(kernel_column, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column, 1, sizeof(cl_mem), &lBuffer);
  clSetKernelArg(kernel_column, 2, sizeof(unsigned int), &dimensionSize);

  // Execute the Cholesky Decomposition using hybrid of CPU and GPU
  //  For this execution, all of the data is on the GPU. However, syncronization is required,
  //  and is done implicitly by queuing different kernels
  // Used OpenCL functions:
  //  https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueNDRangeKernel.html
  size_t workSize[] = { dimensionSize };
  unsigned int col;
  cl_event first_event, last_event;
  size_t globalWorkSize[] = { 0 };
  size_t *localWorkSize = NULL;

  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;

  // Parallel part (limited by cutoff)
  for (col = 0; col < dimensionSize - cutoff; col++) {
    globalWorkSize[0] = dimensionSize - (col + 1) + 1;

    // Set current column argument
    clSetKernelArg(kernel_column, 3, sizeof(int), &col);

    // Queue kernel execution (and check for errors)
    CheckError(clEnqueueNDRangeKernel(
      queue, kernel_column,       // queue and kernel (column)
      1,                  // 1 dimension

      NULL,               // no dimension offsets
      globalWorkSize,           // global work size
      localWorkSize,            // local work size

      0, NULL,              // no need to wait for any events
      (col == 0) ?
        &first_event :          // on the first, use first_event
        (col == dimensionSize - 1 - cutoff) ?
          &last_event :       // on the last run, use last_event
          NULL            // on all others, use null
      ));
  }

  // Queue reading the buffer back (and only return when all of the buffer was copied)
  // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
  CheckError(clEnqueueReadBuffer(
    queue,                      // the queue (do this after executing all the kernels)
    lBuffer,                    // read from matrix L
    CL_TRUE,                    // block read: doesn't return until buffer data has been read
    0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
    L,                        // put on this host pointer
    0, NULL, NULL)              // no events
    );

  // wait for the queue to finish
  clFinish(queue);

  // Sequenial part (based on cutoff)
  float sum, value, diag_value;
  int i, j, k;
  for (col = dimensionSize - cutoff; col < dimensionSize; col++) {
    // Diagonal value
    sum = 0;
    for (k = 0; k < col; k++) {
      sum += L[col * dimensionSize + k] * L[col * dimensionSize + k];
    }
    L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum);

    // Calculate all other rows
    for (i = col + 1; i < dimensionSize; i++) {   // for each row below main diagonal
      sum = 0;
      for (j = 0; j < col; j++) {
        sum += L[i * dimensionSize + j] * L[col * dimensionSize + j];
      }
      L[i * dimensionSize + col] = (1.0 / L[col * dimensionSize + col] *
        (A[i * dimensionSize + col] - sum));
    }
  }

  HPCELO_END_TIMER;

  // Calculate time from the start of the first kernel, to the ending of the last
  cl_ulong time_start, time_end;
  float total_time;
  clGetEventProfilingInfo(first_event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
  clGetEventProfilingInfo(last_event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
  total_time = time_end - time_start;
  //printf("%0.8f,", total_time * 0.000000001f); /// 1000000.0);
  printf("%0.8f\n", HPCELO_GET_TIMER);

  // Release kernel
  clReleaseKernel(kernel_column);
}

void cholesky_gpu_simple_multiple(cl_program program, cl_command_queue queue, cl_mem aBuffer, cl_mem lBuffer, unsigned int dimensionSize,
  float *A, float *L, unsigned int cutoff) {
  cl_int error;

  // Build kernel
  cl_kernel kernel_column1 = clCreateKernel(program, "choleskyColumnSimple1", &error);
  CheckError(error);
  cl_kernel kernel_column2 = clCreateKernel(program, "choleskyColumnSimple2", &error);
  CheckError(error);

  // Set Arguments
  clSetKernelArg(kernel_column1, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column1, 1, sizeof(cl_mem), &lBuffer);
  clSetKernelArg(kernel_column1, 2, sizeof(dimensionSize), &dimensionSize);
  clSetKernelArg(kernel_column2, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column2, 1, sizeof(cl_mem), &lBuffer);
  clSetKernelArg(kernel_column2, 2, sizeof(dimensionSize), &dimensionSize);

  // Execute the Cholesky Decomposition using hybrid of CPU and GPU
  //  For this execution, all of the data is on the GPU. However, syncronization is required,
  //  and is done implicitly by queuing different kernels
  // Used OpenCL functions:
  //  https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueNDRangeKernel.html
  size_t workSize[] = { dimensionSize };
  unsigned int col;
  cl_event first_event = NULL, last_event = NULL;
  size_t globalWorkSize1[] = { 1 };
  size_t globalWorkSize2[] = { 0 };
  size_t *localWorkSize = NULL;

  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;

  // if no cutoff
  if (cutoff <= 0) {
    // Execute: two kernels until last iteration
    for (col = 0; col < dimensionSize - 1; col++) {
      globalWorkSize2[0] = dimensionSize - (col + 1) + 1 - 1;

      // Set current column argument
      clSetKernelArg(kernel_column1, 3, sizeof(col), &col);
      clSetKernelArg(kernel_column2, 3, sizeof(col), &col);

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column1,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize1,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == 0) ?
        &first_event :          // on the first, use first_event
        NULL            // on all others, use null
        ));

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column2,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize2,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        NULL
        ));
    }

    // Queue last first kernel execution (and check for errors)
    CheckError(clEnqueueNDRangeKernel(
      queue, kernel_column1,        // queue and kernel (column)
      1,                  // 1 dimension

      NULL,               // no dimension offsets
      globalWorkSize1,            // global work size
      localWorkSize,            // local work size

      0, NULL,              // no need to wait for any events
      &last_event
      ));

    // Queue reading the buffer back (and only return when all of the buffer was copied)
    // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
    CheckError(clEnqueueReadBuffer(
      queue,                      // the queue (do this after executing all the kernels)
      lBuffer,                    // read from matrix A
      CL_TRUE,                    // block read: doesn't return until buffer data has been read
      0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
      L,                        // put on this host pointer
      1, &last_event, NULL)              // no events
      );

    // wait for the queue to finish
    clFinish(queue);
  }
  else {
    // Parallel part (limited by cutoff)
    for (col = 0; col < dimensionSize - cutoff; col++) {
      globalWorkSize2[0] = dimensionSize - (col + 1) + 1 - 1;

      // Set current column argument
      clSetKernelArg(kernel_column1, 3, sizeof(col), &col);
      clSetKernelArg(kernel_column2, 3, sizeof(col), &col);

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column1,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize1,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == 0) ?
        &first_event :          // on the first, use first_event
        NULL            // on all others, use null
        ));

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column2,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize2,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == dimensionSize - 1 - cutoff) ?
        &last_event :       // on the last run, use last_event
        NULL            // on all others, use null
        ));
    }

    // Queue reading the buffer back (and only return when all of the buffer was copied)
    // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
    CheckError(clEnqueueReadBuffer(
      queue,                      // the queue (do this after executing all the kernels)
      lBuffer,                    // read from matrix A
      CL_TRUE,                    // block read: doesn't return until buffer data has been read
      0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
      L,                        // put on this host pointer
      1, &last_event, NULL)              // no events
      );

    // wait for the queue to finish
    clFinish(queue);

    // Sequenial part (based on cutoff)
    float sum, value, diag_value;
    int i, j, k;
    for (col = dimensionSize - cutoff; col < dimensionSize; col++) {
      sum = 0;
      for (k = 0; k < col; k++) {
        sum += L[col * dimensionSize + k] * L[col * dimensionSize + k];
      }
      L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum);

      // Calculate all other rows
      for (i = col + 1; i < dimensionSize; i++) {   // for each row below main diagonal
        sum = 0;
        for (j = 0; j < col; j++) {
          sum += L[i * dimensionSize + j] * L[col * dimensionSize + j];
        }
        L[i * dimensionSize + col] = (1.0 / L[col * dimensionSize + col] *
          (A[i * dimensionSize + col] - sum));
      }
    }
  }

  HPCELO_END_TIMER;

  // Calculate time from the start of the first kernel, to the ending of the last
  cl_ulong time_start, time_end;
  float total_time;
  clGetEventProfilingInfo(first_event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
  clGetEventProfilingInfo(last_event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
  total_time = time_end - time_start;
  //printf("%0.8f,", total_time * 0.000000001f); /// 1000000.0);
  printf("%0.8f\n", HPCELO_GET_TIMER);

  // Release kernel
  clReleaseKernel(kernel_column1);
  clReleaseKernel(kernel_column2);
}

void cholesky_gpu_inplace_multiple(cl_program program, cl_command_queue queue, cl_mem aBuffer, unsigned int dimensionSize,
  float *A, float *L, unsigned int cutoff) {
  cl_int error;

  // Build kernel
  cl_kernel kernel_column1 = clCreateKernel(program, "choleskyColumnInplace1", &error);
  CheckError(error);
  cl_kernel kernel_column2 = clCreateKernel(program, "choleskyColumnInplace2", &error);
  CheckError(error);

  // Set Arguments
  clSetKernelArg(kernel_column1, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column1, 1, sizeof(dimensionSize), &dimensionSize);
  clSetKernelArg(kernel_column2, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column2, 1, sizeof(dimensionSize), &dimensionSize);

  // Execute the Cholesky Decomposition using hybrid of CPU and GPU
  //  For this execution, all of the data is on the GPU. However, syncronization is required,
  //  and is done implicitly by queuing different kernels
  // Used OpenCL functions:
  //  https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueNDRangeKernel.html
  size_t workSize[] = { dimensionSize };
  unsigned int col;
  cl_event first_event = NULL, last_event = NULL;
  size_t globalWorkSize1[] = { 1 };
  size_t globalWorkSize2[] = { 0 };
  size_t *localWorkSize = NULL;

  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;

  if (cutoff <= 0) {
  // Execute two kernels until last iteration
    for (col = 0; col < dimensionSize - 1; col++) {
      globalWorkSize2[0] = dimensionSize - (col + 1) + 1 - 1;

      // Set current column argument
      clSetKernelArg(kernel_column1, 2, sizeof(col), &col);
      clSetKernelArg(kernel_column2, 2, sizeof(col), &col);

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column1,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize1,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == 0) ?
        &first_event :          // on the first, use first_event
        NULL            // on all others, use null
      ));

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column2,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize2,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        NULL
      ));
    }

    // Queue last first kernel execution (and check for errors)
    CheckError(clEnqueueNDRangeKernel(
      queue, kernel_column1,        // queue and kernel (column)
      1,                  // 1 dimension

      NULL,               // no dimension offsets
      globalWorkSize1,            // global work size
      localWorkSize,            // local work size

      0, NULL,              // no need to wait for any events
      &last_event
    ));

    // Queue reading the buffer back (and only return when all of the buffer was copied)
    // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
    CheckError(clEnqueueReadBuffer(
      queue,                      // the queue (do this after executing all the kernels)
      aBuffer,                    // read from matrix A
      CL_TRUE,                    // block read: doesn't return until buffer data has been read
      0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
      L,                        // put on this host pointer
      1, &last_event, NULL)              // no events
      );

    // wait for the queue to finish
    clFinish(queue);

  } else {
    // Parallel part (limited by cutoff)
    for (col = 0; col < dimensionSize - cutoff; col++) {
      globalWorkSize2[0] = dimensionSize - (col + 1) + 1 - 1;

      // Set current column argument
      clSetKernelArg(kernel_column1, 2, sizeof(col), &col);
      clSetKernelArg(kernel_column2, 2, sizeof(col), &col);

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column1,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize1,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == 0) ?
        &first_event :          // on the first, use first_event
        NULL            // on all others, use null
        ));

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column2,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize2,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == dimensionSize - 1 - cutoff) ?
        &last_event :       // on the last run, use last_event
        NULL            // on all others, use null
        ));
    }

    // Queue reading the buffer back (and only return when all of the buffer was copied)
    // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
    CheckError(clEnqueueReadBuffer(
      queue,                      // the queue (do this after executing all the kernels)
      aBuffer,                    // read from matrix A
      CL_TRUE,                    // block read: doesn't return until buffer data has been read
      0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
      L,                        // put on this host pointer
      1, &last_event, NULL)              // no events
      );

    // wait for the queue to finish
    clFinish(queue);

    // Sequenial part (based on cutoff)
    float sum, value, diag_value;
    int i, j, k;
    for (col = dimensionSize - cutoff; col < dimensionSize; col++) {
      sum = 0;
      for (k = 0; k < col; k++) {
        sum += L[col * dimensionSize + k] * L[col * dimensionSize + k];
      }
      L[col * dimensionSize + col] = sqrt(L[col * dimensionSize + col] - sum);

      // Calculate all other rows
      for (i = col + 1; i < dimensionSize; i++) {   // for each row below main diagonal
        sum = 0;
        for (j = 0; j < col; j++) {
          sum += L[i * dimensionSize + j] * L[col * dimensionSize + j];
        }
        L[i * dimensionSize + col] = (1.0 / L[col * dimensionSize + col] *
          (L[i * dimensionSize + col] - sum));
      }
    }
  }

  HPCELO_END_TIMER;

  int i, j;
  for (i = 0; i < dimensionSize; i++) {
    for (j = 0; j < i; j++) {
      L[j * dimensionSize + i] = 0;
    }
  }

  // Calculate time from the start of the first kernel, to the ending of the last
  cl_ulong time_start, time_end;
  float total_time;
  clGetEventProfilingInfo(first_event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
  clGetEventProfilingInfo(last_event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
  total_time = time_end - time_start;
  //printf("%0.8f,", total_time * 0.000000001f); /// 1000000.0);
  printf("%0.8f\n", HPCELO_GET_TIMER);

  // Release kernel
  clReleaseKernel(kernel_column1);
  clReleaseKernel(kernel_column2);
}

void cholesky_gpu_dsum_multiple(cl_program program, cl_command_queue queue, cl_mem aBuffer, cl_mem lBuffer, unsigned int dimensionSize, 
  float *A, float *L, unsigned int cutoff) {
  cl_int error;

  // Build kernel
  cl_kernel kernel_column1 = clCreateKernel(program, "choleskyColumnDSum1", &error);
  CheckError(error);
  cl_kernel kernel_column2 = clCreateKernel(program, "choleskyColumnDsum2", &error);
  CheckError(error);

  // Set Arguments
  clSetKernelArg(kernel_column1, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column1, 1, sizeof(cl_mem), &lBuffer);
  clSetKernelArg(kernel_column1, 2, sizeof(dimensionSize), &dimensionSize);
  clSetKernelArg(kernel_column2, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column2, 1, sizeof(cl_mem), &lBuffer);
  clSetKernelArg(kernel_column2, 2, sizeof(dimensionSize), &dimensionSize);

  // Execute the Cholesky Decomposition using hybrid of CPU and GPU
  //  For this execution, all of the data is on the GPU. However, syncronization is required,
  //  and is done implicitly by queuing different kernels
  // Used OpenCL functions:
  //  https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueNDRangeKernel.html
  size_t workSize[] = { dimensionSize };
  unsigned int col;
  cl_event first_event = NULL, last_event = NULL;
  size_t globalWorkSize1[] = { 1 };
  size_t globalWorkSize2[] = { 0 };
  size_t *localWorkSize = NULL;

  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;

  if (cutoff <= 0) {
    // Execute 2 kernels until last iteration
    for (col = 0; col < dimensionSize - 1; col++) {
      globalWorkSize2[0] = dimensionSize - (col + 1) + 1 - 1;

      // Set current column argument
      clSetKernelArg(kernel_column1, 3, sizeof(col), &col);
      clSetKernelArg(kernel_column2, 3, sizeof(col), &col);

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column1,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize1,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == 0) ?
        &first_event :          // on the first, use first_event
        NULL            // on all others, use null
      ));

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column2,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize2,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        NULL
      ));
    }

    // Queue kernel execution (and check for errors)
    CheckError(clEnqueueNDRangeKernel(
      queue, kernel_column1,        // queue and kernel (column)
      1,                  // 1 dimension

      NULL,               // no dimension offsets
      globalWorkSize1,            // global work size
      localWorkSize,            // local work size

      0, NULL,              // no need to wait for any events
      &last_event
    ));

    // Queue reading the buffer back (and only return when all of the buffer was copied)
    // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
    CheckError(clEnqueueReadBuffer(
      queue,                      // the queue (do this after executing all the kernels)
      lBuffer,                    // read from matrix A
      CL_TRUE,                    // block read: doesn't return until buffer data has been read
      0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
      L,                        // put on this host pointer
      1, &last_event, NULL)              // no events
      );

    // wait for the queue to finish
    clFinish(queue);
  } else {
    // Parallel part (limited by cutoff)
    for (col = 0; col < dimensionSize - cutoff; col++) {
      globalWorkSize2[0] = dimensionSize - (col + 1) + 1 - 1;

      // Set current column argument
      clSetKernelArg(kernel_column1, 3, sizeof(col), &col);
      clSetKernelArg(kernel_column2, 3, sizeof(col), &col);

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column1,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize1,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == 0) ?
        &first_event :          // on the first, use first_event
        NULL            // on all others, use null
        ));

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column2,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize2,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == dimensionSize - 1 - cutoff) ?
        &last_event :       // on the last run, use last_event
        NULL            // on all others, use null
        ));
    }

    // Queue reading the buffer back (and only return when all of the buffer was copied)
    // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
    CheckError(clEnqueueReadBuffer(
      queue,                      // the queue (do this after executing all the kernels)
      lBuffer,                    // read from matrix A
      CL_TRUE,                    // block read: doesn't return until buffer data has been read
      0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
      L,                        // put on this host pointer
      1, &last_event, NULL)              // no events
      );

    // wait for the queue to finish
    clFinish(queue);

    // Sequenial part (based on cutoff)
    float sum, value, diag_value;
    int i, j, k;
    for (col = dimensionSize - cutoff; col < dimensionSize; col++) {

      diag_value = sqrt(A[col * dimensionSize + col] - 
        L[col * dimensionSize + col]);

      L[col * dimensionSize + col] = diag_value;

      diag_value = 1.0 / diag_value;

      // Calculate all other rows
      for (i = col + 1; i < dimensionSize; i++) {   // for each row below main diagonal
        sum = 0;
        for (j = 0; j < col; j++) {
          sum += L[i * dimensionSize + j] * L[col * dimensionSize + j];
        }
        value = (diag_value * (A[i * dimensionSize + col] - sum));

        L[i * dimensionSize + col] = value;
        L[i * dimensionSize + i] = value * value;
      }
    }
  }

  HPCELO_END_TIMER;

  // Calculate time from the start of the first kernel, to the ending of the last
  cl_ulong time_start, time_end;
  float total_time;
  clGetEventProfilingInfo(first_event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
  clGetEventProfilingInfo(last_event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
  total_time = time_end - time_start;
  //printf("%0.8f,", total_time * 0.000000001f); /// 1000000.0);
  printf("%0.8f\n", HPCELO_GET_TIMER);

  // Release kernel
  clReleaseKernel(kernel_column1);
  clReleaseKernel(kernel_column2);
}

void cholesky_gpu_unroll_single(cl_program program, cl_command_queue queue, cl_mem aBuffer, cl_mem lBuffer, unsigned int dimensionSize, 
  float *A, float *L, unsigned int cutoff) {
  cl_int error;

  // Build kernel
  cl_kernel kernel_column = clCreateKernel(program, "choleskyColumnUnroll", &error);
  CheckError(error);

  // Set Arguments
  clSetKernelArg(kernel_column, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column, 1, sizeof(cl_mem), &lBuffer);
  clSetKernelArg(kernel_column, 2, sizeof(unsigned int), &dimensionSize);

  // Execute the Cholesky Decomposition using hybrid of CPU and GPU
  //  For this execution, all of the data is on the GPU. However, syncronization is required,
  //  and is done implicitly by queuing different kernels
  // Used OpenCL functions:
  //  https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueNDRangeKernel.html
  size_t workSize[] = { dimensionSize };
  unsigned int col;
  cl_event first_event, last_event;
  size_t globalWorkSize[] = { 0 };
  size_t *localWorkSize = NULL;

  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;

  // Parallel part (limited by cutoff)
  for (col = 0; col < dimensionSize - cutoff; col++) {
    globalWorkSize[0] = ceil((float)(dimensionSize - (col + 1) + 1) * 0.5f);

    // Set current column argument
    clSetKernelArg(kernel_column, 3, sizeof(int), &col);

    // Queue kernel execution (and check for errors)
    CheckError(clEnqueueNDRangeKernel(
      queue, kernel_column,       // queue and kernel (column)
      1,                  // 1 dimension

      NULL,               // no dimension offsets
      globalWorkSize,           // global work size
      localWorkSize,            // local work size

      0, NULL,              // no need to wait for any events
      (col == 0) ?
        &first_event :          // on the first, use first_event
        (col == dimensionSize - 1 - cutoff) ?
          &last_event :       // on the last run, use last_event
          NULL            // on all others, use null
      ));
  }

  // Queue reading the buffer back (and only return when all of the buffer was copied)
  // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
  CheckError(clEnqueueReadBuffer(
    queue,                      // the queue (do this after executing all the kernels)
    lBuffer,                    // read from matrix L
    CL_TRUE,                    // block read: doesn't return until buffer data has been read
    0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
    L,                        // put on this host pointer
    0, NULL, NULL)              // no events
    );

  // wait for the queue to finish
  clFinish(queue);

  // Sequenial part (based on cutoff)
  float sum, value, diag_value;
  int i, j, k;
  for (col = dimensionSize - cutoff; col < dimensionSize; col++) {
    // Diagonal value
    sum = 0;
    for (k = 0; k < col; k++) {
      sum += L[col * dimensionSize + k] * L[col * dimensionSize + k];
    }
    L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum);

    // Calculate all other rows
    for (i = col + 1; i < dimensionSize; i++) {   // for each row below main diagonal
      sum = 0;
      for (j = 0; j < col; j++) {
        sum += L[i * dimensionSize + j] * L[col * dimensionSize + j];
      }
      L[i * dimensionSize + col] = (1.0 / L[col * dimensionSize + col] *
        (A[i * dimensionSize + col] - sum));
    }
  }

  HPCELO_END_TIMER;

  // Calculate time from the start of the first kernel, to the ending of the last
  cl_ulong time_start, time_end;
  float total_time;
  clGetEventProfilingInfo(first_event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
  clGetEventProfilingInfo(last_event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
  total_time = time_end - time_start;
  //printf("%0.8f,", total_time * 0.000000001f); /// 1000000.0);
  printf("%0.8f\n", HPCELO_GET_TIMER);

  // Release kernel
  clReleaseKernel(kernel_column);
}

void cholesky_gpu_unroll_multiple(cl_program program, cl_command_queue queue, cl_mem aBuffer, cl_mem lBuffer, unsigned int dimensionSize, 
  float *A, float *L, unsigned int cutoff) {
  cl_int error;

  // Build kernel
  cl_kernel kernel_column1 = clCreateKernel(program, "choleskyColumnUnroll1", &error);
  CheckError(error);
  cl_kernel kernel_column2 = clCreateKernel(program, "choleskyColumnUnroll2", &error);
  CheckError(error);

  // Set Arguments
  clSetKernelArg(kernel_column1, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column1, 1, sizeof(cl_mem), &lBuffer);
  clSetKernelArg(kernel_column1, 2, sizeof(dimensionSize), &dimensionSize);
  clSetKernelArg(kernel_column2, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel_column2, 1, sizeof(cl_mem), &lBuffer);
  clSetKernelArg(kernel_column2, 2, sizeof(dimensionSize), &dimensionSize);

  // Execute the Cholesky Decomposition using hybrid of CPU and GPU
  //  For this execution, all of the data is on the GPU. However, syncronization is required,
  //  and is done implicitly by queuing different kernels
  // Used OpenCL functions:
  //  https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueNDRangeKernel.html
  size_t workSize[] = { dimensionSize };
  unsigned int col;
  cl_event first_event = NULL, last_event = NULL;
  size_t globalWorkSize1[] = { 1 };
  size_t globalWorkSize2[] = { 0 };
  size_t *localWorkSize = NULL;

  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;

  // if no cutoff, all will be executed on GPU
  if (cutoff <= 0) {
    // Execution. 2 Kernel enqueueing until the last one, which will be only the first
    for (col = 0; col < dimensionSize-1; col++) {
      globalWorkSize2[0] = ceil((float)(dimensionSize - (col + 1) + 1 - 1) * 0.5f);

      // Set current column argument
      clSetKernelArg(kernel_column1, 3, sizeof(col), &col);
      clSetKernelArg(kernel_column2, 3, sizeof(col), &col);

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column1,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize1,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == 0) ?
        &first_event :          // on the first, use first_event
        NULL            // on all others, use null
      ));

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column2,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize2,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        NULL
      ));
    }

    // Last kernel
    // Set current column argument
    clSetKernelArg(kernel_column1, 3, sizeof(col), &col);

    // Queue kernel execution (and check for errors)
    CheckError(clEnqueueNDRangeKernel(
      queue, kernel_column1,        // queue and kernel (column)
      1,                  // 1 dimension

      NULL,               // no dimension offsets
      globalWorkSize1,            // global work size
      localWorkSize,            // local work size

      0, NULL,              // no need to wait for any events
      &last_event
    ));

    // Queue reading the buffer back (and only return when all of the buffer was copied)
    // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
    CheckError(clEnqueueReadBuffer(
      queue,                      // the queue (do this after executing all the kernels)
      lBuffer,                    // read from matrix A
      CL_TRUE,                    // block read: doesn't return until buffer data has been read
      0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
      L,                        // put on this host pointer
      1, &last_event, NULL)              // no events
      );

    // wait for the queue to finish
    clFinish(queue);
  }
  // has cutoff
  else {
    // Parallel part (limited by cutoff)
    for (col = 0; col < dimensionSize - cutoff; col++) {
      globalWorkSize2[0] = ceil((float)(dimensionSize - (col + 1) + 1 - 1) * 0.5f);

      // Set current column argument
      clSetKernelArg(kernel_column1, 3, sizeof(col), &col);
      clSetKernelArg(kernel_column2, 3, sizeof(col), &col);

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column1,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize1,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == 0) ?
        &first_event :          // on the first, use first_event
        NULL            // on all others, use null
        ));

      // Queue kernel execution (and check for errors)
      CheckError(clEnqueueNDRangeKernel(
        queue, kernel_column2,        // queue and kernel (column)
        1,                  // 1 dimension

        NULL,               // no dimension offsets
        globalWorkSize2,            // global work size
        localWorkSize,            // local work size

        0, NULL,              // no need to wait for any events
        (col == dimensionSize - 1 - cutoff) ?
        &last_event :       // on the last run, use last_event
        NULL            // on all others, use null
        ));
    }

    // Queue reading the buffer back (and only return when all of the buffer was copied)
    // https://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clEnqueueReadBuffer.html
    CheckError(clEnqueueReadBuffer(
      queue,                      // the queue (do this after executing all the kernels)
      lBuffer,                    // read from matrix A
      CL_TRUE,                    // block read: doesn't return until buffer data has been read
      0, dimensionSize*dimensionSize*sizeof(float),  // read the whole matrix (from 0 to dimensionSize * dimensionSize)
      L,                        // put on this host pointer
      1, &last_event, NULL)              // no events
      );

    // wait for the queue to finish
    clFinish(queue);

    // Sequenial part (based on cutoff)
    float sum, value, diag_value;
    int i, j, k;
    for (col = dimensionSize - cutoff; col < dimensionSize; col++) {
      sum = 0;
      for (k = 0; k < col; k++) {
        sum += L[col * dimensionSize + k] * L[col * dimensionSize + k];
      }
      L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum);

      // Calculate all other rows
      for (i = col + 1; i < dimensionSize; i++) {   // for each row below main diagonal
        sum = 0;
        for (j = 0; j < col; j++) {
          sum += L[i * dimensionSize + j] * L[col * dimensionSize + j];
        }
        L[i * dimensionSize + col] = (1.0 / L[col * dimensionSize + col] *
          (A[i * dimensionSize + col] - sum));
      }
    }
  }

  HPCELO_END_TIMER;

  // Calculate time from the start of the first kernel, to the ending of the last
  cl_ulong time_start, time_end;
  float total_time;
  clGetEventProfilingInfo(first_event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
  clGetEventProfilingInfo(last_event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
  total_time = time_end - time_start;
  //printf("%0.8f,", total_time * 0.000000001f); /// 1000000.0);
  printf("%0.8f\n", HPCELO_GET_TIMER);

  // Release kernel
  clReleaseKernel(kernel_column1);
  clReleaseKernel(kernel_column2);
}

int main(int argc, char** argv) 
{
  // Read from command line: size, cutoff, chunk size, platform index, device index
  unsigned int dimensionSize = (argc >= 2)?
    atoi(argv[1]) :
    DEFAULT_MATRIX_SIZE;
  int algorithm = (argc >= 3)?
    atoi(argv[2]) :
    0;
  int cutoff = (argc >= 4)?
    atoi(argv[3]) :
    DEFAULT_CUTOFF;
  int platformIndex = (argc >= 5)?
    atoi(argv[4]) :
    0;
  int deviceIndex = (argc >= 6)?
    atoi(argv[5]) :
    0;
  int compareResults = (argc >= 7)?
    atoi(argv[6]) :
    0;

  if (algorithm == 0 && (cutoff > 0))
    return 0; // if sequential, cancel execution if cutoff > 0

  if (cutoff >= dimensionSize)
    return 0; // if cutoff is greater or equals than the input size, cancel execution

  // Initialize OpenCL
  cl_int error = CL_SUCCESS;
  cl_platform_id platform_ids[MAX_OCL_PLATFORMS];
  cl_device_id device_ids[MAX_OCL_DEVICES];
  cl_uint num_devices;
  cl_uint num_platforms;

  CheckError(clGetPlatformIDs(MAX_OCL_PLATFORMS, platform_ids, &num_platforms));
  if (num_platforms == 0 || num_platforms < platformIndex)
    exit(1);
  CheckError(clGetDeviceIDs(platform_ids[platformIndex], 
    CL_DEVICE_TYPE_ALL, MAX_OCL_DEVICES, device_ids, &num_devices));
  if (num_devices == 0 || num_devices < deviceIndex)
    exit(1);

  // Create an OpenCL context
  const cl_context_properties contextProperties[] =
  {
    CL_CONTEXT_PLATFORM, (cl_context_properties)platform_ids[platformIndex],
    0
  };
  cl_context context = clCreateContext(contextProperties, num_devices, 
    device_ids, NULL, NULL, &error);
  CheckError(error);

  // Create Command Queue (only 1 needed)
  cl_command_queue queue = clCreateCommandQueue(
    context, device_ids[deviceIndex],      // in the current context, using the specified device
    CL_QUEUE_PROFILING_ENABLE,          // enables profiling (for accurate time measurements)
    &error
  );
  CheckError(error);

  // Initialize Input and Output Matrices
  unsigned int dimensionSize2 = dimensionSize * dimensionSize;  // shortcut for size squared

  // Build a random, symetric, positive definite matrix (using spd_matrix)
  float *A = spd_create_symetricf(dimensionSize, 1, 100);
  spd_make_positive_definitef (A, dimensionSize, 50000);
  float *L = spd_create_blankf(dimensionSize);

  // Create memory buffers
  cl_mem aBuffer = clCreateBuffer(context,
    CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY,
    sizeof(float) * (dimensionSize2),
    A, &error);
  CheckError(error);
  cl_mem lBuffer = clCreateBuffer(context,
    CL_MEM_COPY_HOST_PTR | CL_MEM_READ_WRITE,
    sizeof(float) * (dimensionSize2),
    L, &error);
  CheckError(error);

  // Build program
  cl_program program = CreateProgramFromKernel("cholesky_kernel.cl", context);
  CheckError(clBuildProgram(program, num_devices,
    device_ids, NULL, NULL, NULL));

  // Execute respective host code
  switch (algorithm) {
  case 0:
    printf("%d,Sequential,%d,", dimensionSize, cutoff);
    cholesky_sequential_timed(A, L, dimensionSize);
    break;

  case 1:
    printf("%d,SimpleS,%d,", dimensionSize, cutoff);
    cholesky_gpu_simple_single(program, queue, aBuffer, lBuffer, dimensionSize, A, L, cutoff);
    break;

  case 2:
    printf("%d,SimpleM,%d,", dimensionSize, cutoff);
    cholesky_gpu_simple_multiple(program, queue, aBuffer, lBuffer, dimensionSize, A, L, cutoff);
     break;

  case 3:
    printf("%d,InPlaceM,%d,", dimensionSize, cutoff);
    cholesky_gpu_inplace_multiple(program, queue, aBuffer, dimensionSize, A, L, cutoff);
    break;

  case 4:
    printf("%d,DSumM,%d,", dimensionSize, cutoff);
    cholesky_gpu_dsum_multiple(program, queue, aBuffer, lBuffer, dimensionSize, A, L, cutoff);
    break;

  case 5:
    printf("%d,UnrollS,%d,", dimensionSize, cutoff);
    cholesky_gpu_unroll_single(program, queue, aBuffer, lBuffer, dimensionSize, A, L, cutoff);
    break;

  case 6:
    printf("%d,UnrollM,%d,", dimensionSize, cutoff);
    cholesky_gpu_unroll_multiple(program, queue, aBuffer, lBuffer, dimensionSize, A, L, cutoff);
    break;
  }
  
  // Compare GPU execution and output with CPU
  if (compareResults > 0) {
    float *L_truth = spd_create_blankf(dimensionSize);

    printf("\n\nInput Matrix: \n");
    spd_print_matrixf(A, dimensionSize, 5);

    printf("\nGPU Output: \n");
    spd_print_matrixf(L, dimensionSize, 5);

    printf("\nCPU Sequential Output: \n");
    cholesky_sequential(A, L_truth, dimensionSize);
    spd_print_matrixf(L_truth, dimensionSize, 5);

    printf("Result: %d\n", spd_comapre_matricesf(L, L_truth, dimensionSize, EPSILON));

    spd_free_matrixf(L_truth);
  }

  // Free stuff
  spd_free_matrixf(A);
  spd_free_matrixf(L);
  clReleaseCommandQueue(queue);
  clReleaseProgram(program);
  clReleaseContext(context);
}

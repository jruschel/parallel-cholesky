/* Sequential Cholesky Implementation Comparison
  Input: <size of square matrix> <algorithm>
  Where algorithm is:
    0: Cholesky-Banachiewicz   DR
    1: Cholesky-Crout          DR
    2: Cholesky-Crout          CO
    3: Cholesky-Crout          CO    T
    4: Cholesky-Crout          CO IP
    5: Cholesky-Crout          CO IP T
  DR: Double Reference
  CO: Contiguous
  IP: In-Place
  T:  Transposed
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "hpcelo.h"
#include "spd_matrix.h"

#define DEFAULT_MATRIX_SIZE 100
HPCELO_DECLARE_TIMER;

int banachiewicz_df(int dimensionSize) {
  int i, j, k;

  // initialize input and output matrices
  float** A = (float**) malloc(dimensionSize * sizeof(float*));
  for (i = 0; i < dimensionSize; i++) {
    A[i] = (float*) malloc(dimensionSize * sizeof(float));
    for (j = 0; j <= i; j++) {
      A[i][j] = ceil(spd_random_float(1, 100));
      if (i == j)
        A[i][j] += 50000;
      else
        A[j][i] = A[i][j];
    }
  }
  float** L = (float**) malloc(dimensionSize * sizeof(float*));
  for (i = 0; i < dimensionSize; i++) {
    L[i] = (float*) malloc(dimensionSize * sizeof(float));
    for (j = 0; j <= i; j++) {
      L[i][j] = 0;
    }
  }

  HPCELO_START_TIMER;

  for (i = 0; i < dimensionSize; i++) {
    for (j = 0; j < (i + 1); j++) {
      float sum = 0;
      for (k = 0; k < j; k++)
        sum += L[i][k] * L[j][k];

      if (i == j)
        L[i][j] = sqrt(A[i][i] - sum);
      else
        L[i][j] = (1.0 / L[j][j] * (A[i][j] - sum));
    }
  }

  HPCELO_END_TIMER;

  // free input and output matrices
  for (i = 0; i < dimensionSize; i++)
    free(A[i]);
  free(A);
  for (i = 0; i < dimensionSize; i++)
    free(L[i]);
  free(L);

  return 0;
}

int crout_df(int dimensionSize) {
  int i, j, k;

  // initialize input and output matrices
  float** A = (float**) malloc(dimensionSize * sizeof(float*));
  for (i = 0; i < dimensionSize; i++) {
    A[i] = (float*) malloc(dimensionSize * sizeof(float));
    for (j = 0; j <= i; j++) {
      A[i][j] = spd_random_float(1, 100);
      if (i == j)
        A[i][j] += 50000;
      else
        A[j][i] = A[i][j];
    }
  }
  float** L = (float**) malloc(dimensionSize * sizeof(float*));
  for (i = 0; i < dimensionSize; i++) {
    L[i] = (float*) malloc(dimensionSize * sizeof(float));
    for (j = 0; j <= i; j++) {
      L[i][j] = 0;
    }
  }

  HPCELO_START_TIMER;

  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[j][k] * L[j][k];
    }
    L[j][j] = sqrt(A[j][j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i][k] * L[j][k];
      }
      L[i][j] = (1.0 / L[j][j] * (A[i][j] - sum));
    }
  }

  HPCELO_END_TIMER;

  // free input and output matrices
  for (i = 0; i < dimensionSize; i++)
    free(A[i]);
  free(A);
  for (i = 0; i < dimensionSize; i++)
    free(L[i]);
  free(L);

  return 0;
}

int crout_co(int dimensionSize) {
  int i, j, k;

  // initialize input and output matrices
  float *A = spd_create_symetricf (dimensionSize, 1, 100);
  spd_make_positive_definitef (A, dimensionSize, 50000);
  float *L = spd_create_uninitf (dimensionSize);

  HPCELO_START_TIMER;

  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[j * dimensionSize + k] * L[j * dimensionSize + k];
    }
    L[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i * dimensionSize + k] * L[j * dimensionSize + k];
      }
      L[i * dimensionSize + j] = (1.0 / L[j * dimensionSize + j] * 
        (A[i * dimensionSize + j] - sum));
    }
  }

  HPCELO_END_TIMER;

  // free input and output matrices
  spd_free_matrixf (A);
  spd_free_matrixf (L);

  return 0;
}

int crout_co_t(int dimensionSize) {
  int i, j, k;

  // initialize input and output matrices
  float *A = spd_create_symetricf (dimensionSize, 1, 100);
  spd_make_positive_definitef (A, dimensionSize, 50000);
  float *L = spd_create_uninitf (dimensionSize);

  HPCELO_START_TIMER;

  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[k * dimensionSize + j] * L[k * dimensionSize + j];
    }
    L[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[k * dimensionSize + i] * L[k * dimensionSize + j];
      }
      L[j * dimensionSize + i] = (1.0 / L[j * dimensionSize + j] * 
        (A[j * dimensionSize + i] - sum));
    }
  }

  HPCELO_END_TIMER;

  // free input and output matrices
  spd_free_matrixf (A);
  spd_free_matrixf (L);

  return 0;
}

int crout_co_ip(int dimensionSize) {
  
  // initialize input and output matrices
  float *A = spd_create_symetricf (dimensionSize, 1, 100);
  spd_make_positive_definitef (A, dimensionSize, 50000);
  
  int i, j, k;

  HPCELO_START_TIMER;
  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += A[j * dimensionSize + k] * A[j * dimensionSize + k];
    }
    A[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += A[i * dimensionSize + k] * A[j * dimensionSize + k];
      }
      A[i * dimensionSize + j] = (1.0 / A[j * dimensionSize + j] * 
        (A[i * dimensionSize + j] - sum));
    }
  }
  HPCELO_END_TIMER;

  spd_free_matrixf (A);

  return 0;
}

int crout_co_ip_t(int dimensionSize) {
  
  // initialize input and output matrices
  float *A = spd_create_symetricf (dimensionSize, 1, 100);
  spd_make_positive_definitef (A, dimensionSize, 50000);
  
  int i, j, k;

  HPCELO_START_TIMER;
  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += A[k * dimensionSize + j] * A[k * dimensionSize + j];
    }
    A[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += A[k * dimensionSize + i] * A[k * dimensionSize + j];
      }
      A[j * dimensionSize + i] = (1.0 / A[j * dimensionSize + j] * 
        (A[j * dimensionSize + i] - sum));
    }
  }
  HPCELO_END_TIMER;

  spd_free_matrixf (A);

  return 0;
}

// Main
int main(int argc, char** argv) {
  // Read matrix size and algorithm from input (or default)
  int input_size = (argc >= 2)?
    atoi(argv[1]) :
    DEFAULT_MATRIX_SIZE;
  int algorithm = (argc >= 3)?
    atoi(argv[2]) :
    0;

  if ((algorithm == 0 || algorithm == 1 || algorithm == 3 || algorithm == 5) && 
    (input_size >= 5000)) return 0;  // too slow

  printf("%d,", input_size);

  switch(algorithm) {
    case 0:
    printf("Banachiewicz,");
    banachiewicz_df(input_size);
    break;

    case 1:
    printf("Crout1,");
    crout_df(input_size);
    break;

    case 2:
    printf("Crout2,");
    crout_co(input_size);
    break;

    case 3:
    printf("Crout3,");
    crout_co_t(input_size);
    break;

    case 4:
    printf("Crout4,");
    crout_co_ip(input_size);
    break;
    
    case 5:
    printf("Crout5,");
    crout_co_ip_t(input_size);
    break;
  }

  printf("%f\n", HPCELO_GET_TIMER);

  return 0;
}

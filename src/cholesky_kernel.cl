/*
	This program contains the kernels used for executing Cholesky's Decomposition on GPU using OpenCL.

	Written by João Paulo T Ruschel, 2016
		Contact: jptruschel@inf.ufrgs.br
*/

#ifdef cl_khr_fp64
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#else
#error "float precision floating point not supported by OpenCL implementation."
#endif

#define EPSILON 0.0000000000001

/* Simple Cholesky Column-oriented Decomposition
	Kernel considers each work item as one row on a single column.
*/
__kernel void choleskyColumnSimple(__global float *A, __global float *L,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id;
	int k;
	float sum = 0;
	float value;
	float sum_d = 0;
	
	if (g_id == 0) {
		for (k = 0; k < col; k++) {
			sum_d += L[col * dimensionSize + k] * L[col * dimensionSize + k];
		}
		L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum_d);
	}
	else {
		for (k = 0; k < col; k++) {
			sum   += L[row * dimensionSize + k] * L[col * dimensionSize + k];
			sum_d += L[col * dimensionSize + k] * L[col * dimensionSize + k];
		}
		value = sqrt(A[col * dimensionSize + col] - sum_d);

		L[row * dimensionSize + col] = (1.0 / value * (A[row * dimensionSize + col] - sum));
	}
}
// Multiple kernels
__kernel void choleskyColumnSimple1(__global float *A, __global float *L,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id;
	int k;
	float sum = 0;

	for (k = 0; k < col; k++) {
		sum += L[row * dimensionSize + k] * L[col * dimensionSize + k];
	}
	L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum);
}
__kernel void choleskyColumnSimple2(__global float *A, __global float *L,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id + 1;
	int k;
	float sum = 0;

	for (k = 0; k < col; k++) {
		sum += L[row * dimensionSize + k] * L[col * dimensionSize + k];
	}

	L[row * dimensionSize + col] = (1.0 / L[col * dimensionSize + col] *
		(A[row * dimensionSize + col] - sum));
}

/* Cholesky Column-oriented Decomposition with In-Place calculation
	Kernel considers each work item as one row on a single column.
*/
// Multiple kernels (single kernel is impossible)
__kernel void choleskyColumnInplace1(__global float *A,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id;
	int k;
	float sum = 0;

	for (k = 0; k < col; k++) {
		sum += A[row * dimensionSize + k] * A[col * dimensionSize + k];
	}
	A[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum);
}
__kernel void choleskyColumnInplace2(__global float *A,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id + 1;
	int k;
	float sum = 0;

	for (k = 0; k < col; k++) {
		sum += A[row * dimensionSize + k] * A[col * dimensionSize + k];
	}

	A[row * dimensionSize + col] = (1.0 / A[col * dimensionSize + col] *
		(A[row * dimensionSize + col] - sum));
}

/* Cholesky Column-oriented Decomposition with distributed sum
	Kernel considers each work item as one row on a single column.
*/
// Multiple kernels
__kernel void choleskyColumnDSum1(__global float *A, __global float *L,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);

	// simple calculation of diagonal element
	L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - L[col * dimensionSize + col]);
}
__kernel void choleskyColumnDsum2(__global float *A, __global float *L,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id + 1;
	int k;
	float sum = 0;
	float value;

	// default loop through row and element calculation
	for (k = 0; k < col; k++) {
		sum += L[row * dimensionSize + k] * L[col * dimensionSize + k];
	}
	value = (1.0 / L[col * dimensionSize + col] *
		(A[row * dimensionSize + col] - sum));
	L[row * dimensionSize + col] = value;

	// adds its square to the diagonal element of this row 
	L[row * dimensionSize + row] = value * value;
}

/* Cholesky Column-oriented Decomposition with unrolling 2
	Kernel considers each work item as two rows on a single column.
*/
__kernel void choleskyColumnUnroll(__global float *A, __global float *L,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id;
	int k;
	float sum1 = 0, sum2 = 0;
	float value;
	float sum_d = 0;
	
	// diagonal
	if (g_id == 0) {
		// check if needs to process 2 elements or just 1
		if (row + 1 < dimensionSize) {
			// process diagonal +1
			for (k = 0; k < col; k++) {
				value = L[col * dimensionSize + k];		// only 1 access
				sum_d += value * value;
				sum2 += L[(row+1) * dimensionSize + k] * value;
			}
			value = sqrt(A[col * dimensionSize + col] - sum_d);
			L[col * dimensionSize + col] = value;
			L[(row+1) * dimensionSize + col] = (1.0 / value * (A[(row+1) * dimensionSize + col] - sum2));
		} else {
			// process only diagonal
			for (k = 0; k < col; k++) {
				value = L[col * dimensionSize + k];		// only 1 access
				sum_d += value * value;
			}
			L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum_d);
		}
	}
	// other elements
	else {
		// check if needs to process 2 elements or just 1
		if (row + 1 < dimensionSize) {
			// process 2 elements

			for (k = 0; k < col; k++) {
				value = L[col * dimensionSize + k];		// only 1 access
				sum1   += L[row * dimensionSize + k] * value;
				sum2  += L[(row+1) * dimensionSize + k] * value;
				sum_d += value * value;
			}
			value = 1.0 / sqrt(A[col * dimensionSize + col] - sum_d);

			L[row * dimensionSize + col] = (value * (A[row * dimensionSize + col] - sum1));
			L[(row+1) * dimensionSize + col] = (value * (A[(row+1) * dimensionSize + col] - sum1));
		} else {
			// process only 1 element

			for (k = 0; k < col; k++) {
				value = L[col * dimensionSize + k];		// only 1 access
				sum1   += L[row * dimensionSize + k] * value;
				sum_d += value * value;
			}
			value = sqrt(A[col * dimensionSize + col] - sum_d);

			L[row * dimensionSize + col] = (1.0 / value * (A[row * dimensionSize + col] - sum1));
		}
	}
}
// Multiple kernels
__kernel void choleskyColumnUnroll1(__global float *A, __global float *L,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id;
	int k;
	float sum = 0;

	for (k = 0; k < col; k++) {
		sum += L[row * dimensionSize + k] * L[col * dimensionSize + k];
	}
	L[col * dimensionSize + col] = sqrt(A[col * dimensionSize + col] - sum);
}
__kernel void choleskyColumnUnroll2(__global float *A, __global float *L,
	const unsigned int dimensionSize, const unsigned int col)
{
	const int g_id = get_global_id(0);
	int row = col + g_id + 1;
	int k;
	float sum1 = 0, sum2 = 0;
	float value;

	if (row + 1 < dimensionSize) {
		// process two elements

		for (k = 0; k < col; k++) {
			value = L[col * dimensionSize + k];		// single access

			sum1 += L[row * dimensionSize + k] * value;
			sum2 += L[(row+1) * dimensionSize + k] * value;
		}

		value = 1.0 / L[col * dimensionSize + col];

		L[row * dimensionSize + col] = (value  * (A[row * dimensionSize + col] - sum1));

		L[(row+1) * dimensionSize + col] = (value * (A[(row+1) * dimensionSize + col] - sum2));

	} else {
		// only 1 needs to be processed

		for (k = 0; k < col; k++) {
			sum1 += L[row * dimensionSize + k] * L[col * dimensionSize + k];
		}

		L[row * dimensionSize + col] = (1.0 / L[col * dimensionSize + col] *
			(A[row * dimensionSize + col] - sum1));

	}
}

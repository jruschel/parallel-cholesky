/* CPU Parallel Cholesky Implementation with OpenMP.
  Input: <size of square matrix> <algorithm> <cutoff> <number of threads>
  Where algorithm:
    0: sequential (Cholesky-Crout)
    1: Parallel Simple
    2: Parallel Distributed Sum
    3: Parallel InPlace
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "omp.h"
#include "hpcelo.h"
#include "spd_matrix.h"

/* The algorithms here were already tested with a few ground truths to verify accuracy.
  If further testing is needed, enable the next line. */
//#define COMPARE_RESULTS

#define DEFAULT_MATRIX_SIZE 100

// Error
#define EPSILON 0.0000000000001

/* Babylonian method for calculating the square root.
  if the input value is negative, will return -1 (error). */
inline float c_sqrt(const float fg)
{
  return sqrt(fg);

  if (fg <= -EPSILON) return -1.0;
  float n = fg / 2.0;
  float lstX = 0.0;
  while (fabs(n - lstX) > EPSILON)
  {
    lstX = n;
    n = (n + fg / n) / 2.0;
  }
  return n;
}

HPCELO_DECLARE_TIMER;

// Sequential implementation of Cholesky-Crout Algorithm
int cholesky_sequential(float *A, float *L, unsigned int dimensionSize) {
  if (A == NULL || L == NULL)
    return 1;

  float sum, value, diagonal_value;
  int i, j, k;

  HPCELO_START_TIMER;
  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[j * dimensionSize + k] * L[j * dimensionSize + k];
    }
    L[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i * dimensionSize + k] * L[j * dimensionSize + k];
      }
      L[i * dimensionSize + j] = (1.0 / L[j * dimensionSize + j] * 
        (A[i * dimensionSize + j] - sum));
    }
  }
  HPCELO_END_TIMER;

  return 0;
}

// Parallel implementation of Cholesky-Crout algorithm, with cutoff
int cholesky_simple(float *A, float *L, int dimensionSize, int cutoff) {
  if (A == NULL || L == NULL)
    return 1;

  float sum, value, diagonal_value;
  int i, j, k;

  HPCELO_START_TIMER;
  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[j * dimensionSize + k] * L[j * dimensionSize + k];
    }
    L[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    #pragma omp parallel for private(i,k,sum) shared (A,L,j,diagonal_value) schedule(static) if (j < dimensionSize - cutoff)
    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i * dimensionSize + k] * L[j * dimensionSize + k];
      }
      L[i * dimensionSize + j] = (1.0 / L[j * dimensionSize + j] * 
        (A[i * dimensionSize + j] - sum));
    }
  }
  HPCELO_END_TIMER;

  return 0;
}

// Parallel implementation of Cholesky-Crout algorithm, with cutoff
int cholesky_simple_ds(float *A, float *L, int dimensionSize, int cutoff) {
  if (A == NULL || L == NULL)
    return 1;

  float sum, value, diagonal_value;
  int i, j, k;

  HPCELO_START_TIMER;
  for (j = 0; j < dimensionSize; j++) {
    diagonal_value = sqrt(A[j * dimensionSize + j] - L[j * dimensionSize + j]);
    L[j * dimensionSize + j] = diagonal_value;

    diagonal_value = 1.0 / diagonal_value;

    #pragma omp parallel for private(i,k,sum) shared (A,L,j,diagonal_value) schedule(static) if (j < dimensionSize - cutoff)
    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i * dimensionSize + k] * L[j * dimensionSize + k];
      }
      value = (diagonal_value * (A[i * dimensionSize + j] - sum));
      L[i * dimensionSize + j] = value;
      L[i * dimensionSize + i] += value * value;
    }
  }
  HPCELO_END_TIMER;

  return 0;
}

// Parallel in-place implementation of Cholesky-Crout algorithm, with cutoff
int cholesky_inplace(float *A, float *L, int dimensionSize, int cutoff) {
  if (A == NULL || L == NULL)
    return 1;

  float sum, value, diagonal_value;
  int i, j, k;

  // copy contents from input matrix to output matrix
  //  this is done to simplify the code and potential testing
  //  and should not be counted as algorithm time
  for (i = 0; i < dimensionSize * dimensionSize; i++)
    L[i] = A[i];

  HPCELO_START_TIMER;
  for (j = 0; j < dimensionSize; j++) {
    float sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[j * dimensionSize + k] * L[j * dimensionSize + k];
    }
    L[j * dimensionSize + j] = sqrt(L[j * dimensionSize + j] - sum);

    #pragma omp parallel for private(i,k,sum) shared (L,j,diagonal_value) schedule(static) if (j < dimensionSize - cutoff)
    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i * dimensionSize + k] * L[j * dimensionSize + k];
      }
      L[i * dimensionSize + j] = (1.0 / L[j * dimensionSize + j] * 
        (L[i * dimensionSize + j] - sum));
    }
  }
  HPCELO_END_TIMER;

  // reset upper triangle
  //  this is done to simplify the code and potential testing
  //  and should not be counted as algorithm time
  for (i = 0; i < dimensionSize; i++) {
    for (j = 0; j < i; j++) {
      L[j * dimensionSize + i] = 0;
    }
  }

  return 0;
}

// Main
int main(int argc, char** argv) {
  // Read input: matrix size, algorithm, and number of threads
  int input_size = (argc >= 2)?
    atoi(argv[1]) :
    DEFAULT_MATRIX_SIZE;
  int algorithm = (argc >= 3)?
    atoi(argv[2]) :
    0;
  int cutoff = (argc >= 4)?
    atoi(argv[3]) :
    0;
  int num_threads = (argc >= 5)?
    atoi(argv[4]) :
    -1;

  if (algorithm < 0) return 0;
  if (algorithm > 3) return 0;

  if ((algorithm == 0) && (num_threads > 1 || cutoff > 0))
    return 0; // if sequential, cancel execution if num_threads > 1 or cutoff > 0

  // Configure OpenMP to use the specified number of threads
  if (num_threads > 0)
    omp_set_num_threads(num_threads);

  // Initialize matrices
  float *A = spd_create_symetricf (input_size, 1, 100);
  spd_make_positive_definitef (A, input_size, 50000);
  float *L = spd_create_blankf (input_size);

  // Print input parameters and run specified cholesky algorithm
  int error_code;
  switch (algorithm) {
  case 0:
    printf("%d,Sequential,%d,%d,", input_size, cutoff, omp_get_max_threads());
    error_code = cholesky_sequential(A, L, input_size);
    break;
  case 1:
    printf("%d,Parallel1,%d,%d,", input_size, cutoff, omp_get_max_threads());
    error_code = cholesky_simple(A, L, input_size, cutoff);
    break;
  case 2:
    printf("%d,Parallel2,%d,%d,", input_size, cutoff, omp_get_max_threads());
    error_code = cholesky_simple_ds(A, L, input_size, cutoff);
    break;
  case 3:
    printf("%d,Parallel3,%d,%d,", input_size, cutoff, omp_get_max_threads());
    error_code = cholesky_inplace(A, L, input_size, cutoff);
    break;
  }
  if (error_code != 0) {
    fprintf (stderr, " Error: error during parallel calculation.\n");
  }
    
  // print ellapsed time
  printf("%f\n", HPCELO_GET_TIMER);

  // Calculates sequentially for error checking (if defined)
  #ifdef COMPARE_RESULTS
    float *L_TRUTH = spd_create_blankf (input_size);  
    if (!cholesky_sequential(A, L_TRUTH, input_size)) {
      if (hpcelo_compare_matrices(L, L_TRUTH, input_size) != 0) {
        fprintf (stderr, " Error: wrong calculation.\n");
      }
    } else {
      fprintf (stderr, " Error: error during sequential calculation.\n");
    }
    // Print start of both matrices
    printf("\nInput Matrix: \n");
    spd_print_matrixf(A, input_size, 8);
    printf("\nAlgorithm Output: \n");
    spd_print_matrixf(L, input_size, 8);

    printf("\nTruth Output: \n");
    spd_print_matrixf(L_TRUTH, input_size, 8);
    hpcelo_free_matrixf (L_TRUTH);
  #endif

  // Free matrices
  spd_free_matrixf (A);
  spd_free_matrixf (L);

  return 0;
}

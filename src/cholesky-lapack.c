/* Cholesky Factorization using LAPACK.
  Input: <size of square matrix>
*/

#include "stdio.h"
#include "stdlib.h"
#include "sys/time.h"
#include "time.h"
#include "omp.h"
#include "hpcelo.h"
#include "spd_matrix.h"

//#define VERBOSE

#define DEFAULT_MATRIX_SIZE 100

// Lapack extern call to Cholesky decomposition
extern int spotrf_(char* uplo, int* n, float* a, int* lda, int* info);

HPCELO_DECLARE_TIMER;

int main(int argc, char** argv) {
  // Read input: matrix size and number of threads
  int size = (argc >= 2)?
    atoi(argv[1]) :
    DEFAULT_MATRIX_SIZE;

  int info = 0;
  char uplo = 'U';

  // Initialize matrices
  float *A = spd_create_symetricf (size, 1, 100);
  spd_make_positive_definitef (A, size, 50000);

  #ifdef VERBOSE
  printf("\nInput Matrix: \n");
  spd_print_matrixf (A, size, 8);
  #endif

  printf("%d,LAPACK,%d,", size, omp_get_max_threads());

  HPCELO_START_TIMER;
  spotrf_(&uplo, &size, A, &size, &info);
  HPCELO_END_TIMER;

  printf("%f\n", HPCELO_GET_TIMER);

  #ifdef VERBOSE
  printf("\nOutput Matrix: \n");
  spd_print_matrixf (A, size, 8);
  #endif

  // Free matrices
  spd_free_matrixf (A);

  return 0;
}

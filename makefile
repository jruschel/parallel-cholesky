CC=gcc

CFLAGS=-O3 -lm
MPFLAGS=-fopenmp
CLFLAGS=-I/usr/local/cuda-8.0/include -lOpenCL
LAPACKFLAG=-llapack

SRC=src
BIN=bin
TST=tests
LIB=lib
LIB_HPC=$(BIN)/hpcelo
LIB_SPM=$(BIN)/spd_matrix

all: hpcelo spd_matrix cholesky_seq cholesky_omp cholesky_ocl cholesky_ocl_test cholesky_lapack

omp: hpcelo spd_matrix cholesky_seq cholesky_omp

ocl: hpcelo spd_matrix cholesky_ocl cholesky_ocl_test

lapack: hpcelo spd_matrix cholesky_lapack

hpcelo:
	$(CC) -o $(BIN)/hpcelo -c lib/hpcelo.c $(CFLAGS)

spd_matrix:
	$(CC) -o $(BIN)/spd_matrix -c lib/spd_matrix.c $(CFLAGS)

cholesky_seq:
	$(CC) $(SRC)/cholesky-seq.c -o $(BIN)/cholesky-seq $(LIB_HPC) $(LIB_SPM) -I$(LIB) $(CFLAGS)

cholesky_omp:
	$(CC) $(SRC)/cholesky-omp.c -o $(BIN)/cholesky-omp $(LIB_HPC) $(LIB_SPM) -I$(LIB) $(CFLAGS) $(MPFLAGS)

cholesky_lapack:
	$(CC) $(SRC)/cholesky-lapack.c -o $(BIN)/cholesky-lapack $(LIB_HPC) $(LIB_SPM) -I$(LIB) $(LAPACKFLAG) $(MPFLAGS) $(CFLAGS)

cholesky_ocl:
	$(CC) $(SRC)/cholesky-ocl.c -o $(BIN)/cholesky-ocl $(LIB_HPC) $(LIB_SPM) -I$(LIB) $(CFLAGS) $(CLFLAGS)
	cp $(SRC)/cholesky_kernel.cl $(BIN)/cholesky_kernel.cl
	cp $(SRC)/cholesky_kernel.cl $(TST)/cholesky_kernel.cl

.PHONY: clean

clean:
	rm -f $(BIN)/*

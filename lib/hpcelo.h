/*
    This file is part of libhpcelo

    libhpcelo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libhpcelo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Public License for more details.

    You should have received a copy of the GNU Public License
    along with libhpcelo. If not, see <http://www.gnu.org/licenses/>.
*/

/*
    Modified from original version by João Paulo T Ruschel, 2016
*/

#ifndef __HPCELO_H_
#define __HPCELO_H_
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define HPCELO_DECLARE_TIMER double hpcelo_t1, hpcelo_t2;
#define HPCELO_START_TIMER  hpcelo_t1 = hpcelo_gettime();
#define HPCELO_END_TIMER    hpcelo_t2 = hpcelo_gettime();
#define HPCELO_GET_TIMER    hpcelo_t2-hpcelo_t1
#define HPCELO_REPORT_TIMER {printf("HPCELO:%f\n", HPCELO_GET_TIMER);}

/* 
 * hpcelo_gettime: this function returns the current time in seconds
 * with a microsecond resolution. It uses =gettimeofday= call. 
 */
double hpcelo_gettime (void);

/* 
 * hpcelo_create_matrix: this function dynamically allocates a matrix
 * with size*size, considering the argument size. Values are not initialized.
 */
double *hpcelo_create_matrix (unsigned long long size);

/* 
 * hpcelo_create_normal_matrix: this function dynamically allocates a matrix
 * with size*size, considering the argument size. After this, it
 * initializes the matrix using a pseudo-random number generator known
 * as linear congruential generator (see wikipedia for details).
 */
double *hpcelo_create_normal_matrix (unsigned long long size);

/* 
 * hpcelo_read_matrix: reads the file named filename as a binary file made of
 * double numbers. The first bytes are an INT that represent the size of the square matrix.
 */
int hpcelo_read_matrix (char *filename, double **output);

/*
 * hpcelo_free_matrix: this function frees the allocated matrix.
 */
void hpcelo_free_matrix (double *matrix);

/*
 * hpcelo_transpose_matrix: this function transpose a matrix that is contiguous allocated. 
 */
void hpcelo_transpose_matrix (double *matrix, int size);

/*
 * hpcelo_print_matrix: this function prints a matrix that is contiguous allocated.
 */
void hpcelo_print_matrix(double *matrix, int size);

/*
 * hpcelo_compare_matrices: this function compares two matrices, element-by-element.
 * Returns 0 if both matrices are equal.
*/
int hpcelo_compare_matrices(double *A, double *B, int size);

#endif

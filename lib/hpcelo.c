/*
    This file is part of libhpcelo

    libhpcelo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libhpcelo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Public License for more details.

    You should have received a copy of the GNU Public License
    along with libhpcelo. If not, see <http://www.gnu.org/licenses/>.
*/

/*
    Modified from original version by João Paulo T Ruschel, 2016
*/

#include "hpcelo.h"

//next function has been found here:
//http://stackoverflow.com/questions/26237419/faster-than-rand
//all credits to the authors there
static unsigned int g_seed;
static inline int fastrand()
{
  g_seed = (214013*g_seed+2531011);
  return (g_seed>>16)&0x7FFF;
}

double hpcelo_gettime (void)
{
  struct timeval tr;
  gettimeofday(&tr, NULL);
  return (double)tr.tv_sec+(double)tr.tv_usec/1000000;
}

double *hpcelo_create_matrix (unsigned long long size)
{
  double *matrix = (double*)calloc(size*size, sizeof(double));

  return matrix;
}

double *hpcelo_create_normal_matrix (unsigned long long size)
{
  double *matrix = hpcelo_create_matrix(size);

  /* Error checking */
  unsigned long long i, j;
  for (i=0; i < size; i++){
    for (j = 0; j < size; j++){
      matrix[(i*size)+j] = (double) fastrand();
    }
  }
  return matrix;
}

int hpcelo_read_matrix (char *filename, double **output) {
  FILE *f = fopen(filename,"rb");
  int size = -1;
  if (!fread(&size, sizeof(int), 1, f))
    return -1;

  *output = hpcelo_create_matrix(size);

  int i;
  while (fread(&(*output[i++]), sizeof(double), 1, f));
  
  return size;
}


void hpcelo_free_matrix (double *matrix)
{
  free(matrix);
  return;
}


void hpcelo_transpose_matrix (double *matrix, int size)
{
  // Temporary matrix
  double *t_matrix = hpcelo_create_matrix(size);

  int i,j,k;
  // Transpose matrix 
  for (i=0; i<size; i++){
    for (j=0; j<size; j++){
      t_matrix[(i*size)+j] = matrix[(j*size)+i];
    }
  }

  // Original matrix receive transposed matrix
  for (i=0; i<size; i++){
    for (j=0; j<size; j++){
      matrix[(i*size)+j] = t_matrix[(i*size)+j];
    }
  }

  hpcelo_free_matrix(t_matrix);

}

void hpcelo_print_matrix(double *matrix, int size)
{
  int i, j;
  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      printf("%f ",matrix[(i*size)+j]);
    }
    printf("\n");
  }
}

int hpcelo_compare_matrices(double *A, double *B, int size) {
  long i=0;
  long size2 = (long)size * (long)size;
  while (i < size2) {
    if (abs(A[i] - B[i]) > 0.0001) {
      printf("%f != %f\n", A[i], B[i]);
      return 1;
    }
    i++;
  }
  return 0;
}
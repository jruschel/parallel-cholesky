#ifndef SPD_MATRIX
#define SPD_MATRIX

/*
	Speed Matrix Functions - Fast and Easy Matrix Functions

	Original code written by Mark Holland
		Full code available at https://github.com/markholland/cholesky

	Modified for specific purposes by João Paulo T Ruschel, 2016
		Contact: jptruschel@inf.ufrgs.br
		Changes:
			Changed base data structure from float** to double*
			Removed a few unused functions
*/

// double
double *spd_create_uninit(unsigned int dimension);
double *spd_create_blank(unsigned int dimension);
double *spd_create_random(unsigned int dimension, double minValue, double maxValue);
double* spd_create_symetric(unsigned int dimension, double minValue, double maxValue);
double* spd_create_identity_matrix(unsigned int dimension, double diagonalValue);
double* spd_create_transpose(double* A, unsigned int dimension);

double spd_random_double(double fMin, double fMax);
void spd_print_matrix(double* A, unsigned int dimension, int count);

double* spd_make_random_matrix(double* A, unsigned int dimension, double minValue, double maxValue);
double* spd_clone_matrix(double* A, unsigned int dimension);
double* spd_make_symetric(double* A, unsigned int dimension);
double* spd_make_positive_definite(double* A, unsigned int dimension, double offset);

int spd_comapre_matrices(double *A, double *B, int dimension, double epsilon);

void spd_free_matrix(double *A);

// float
float *spd_create_uninitf(unsigned int dimension);
float *spd_create_blankf(unsigned int dimension);
float *spd_create_randomf(unsigned int dimension, float minValue, float maxValue);
float* spd_create_symetricf(unsigned int dimension, float minValue, float maxValue);
float* spd_create_identity_matrixf(unsigned int dimension, float diagonalValue);
float* spd_create_transposef(float* A, unsigned int dimension);

float spd_random_float(float fMin, float fMax);
void spd_print_matrixf(float* A, unsigned int dimension, int count);

float* spd_make_random_matrixf(float* A, unsigned int dimension, float minValue, float maxValue);
float* spd_clone_matrixf(float* A, unsigned int dimension);
float* spd_make_symetricf(float* A, unsigned int dimension);
float* spd_make_positive_definitef(float* A, unsigned int dimension, float offset);

int spd_comapre_matricesf(float *A, float *B, int dimension, float epsilon);

void spd_free_matrixf(float *A);

#endif // SPD_MATRIX

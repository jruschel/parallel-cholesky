#include "spd_matrix.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

/*
	Speed Matrix Functions - Fast and Easy Matrix Functions

	Original code written by Mark Holland
		Full code available at https://github.com/markholland/cholesky

	Modified for specific purposes by João Paulo T Ruschel, 2016
		Contact: jptruschel@inf.ufrgs.br
		Changes:
			Changed base data structure from float** to double*
			Removed a few unused functions
*/

// double
double *spd_create_uninit(unsigned int dimension) 
{
	double *m = (double*) calloc(dimension * dimension, sizeof(double));
	return m;
}

double *spd_create_blank(unsigned int dimension) 
{
	double *m = (double*) calloc(dimension * dimension, sizeof(double));
	unsigned int i;
	for (i = 0; i < dimension*dimension; i++)
		m[i] = 0;
	return m;
}

double *spd_create_random(unsigned int dimension, double minValue, double maxValue) 
{
	double *m = (double*) calloc(dimension * dimension, sizeof(double));
	spd_make_random_matrix(m, dimension, minValue, maxValue);

	return m;
}

double* spd_create_symetric(unsigned int dimension, double minValue, double maxValue) 
{
	double *m = (double*) calloc(dimension * dimension, sizeof(double));
	unsigned int i,j;
	for (i = 0; i < dimension; i++) {
		for (j = 0; j <= i; j++) {
			m[i * dimension + j] = spd_random_double(minValue, maxValue);
			m[j * dimension + i] = m[i * dimension + j];
		}
	}
	return m;
}

double* spd_create_identity_matrix(unsigned int dimension, double diagonalValue)
{
	double* nI = spd_create_uninit(dimension);
	unsigned int i, j;
	for (i = 0; i < dimension; i++)
	{
		for (j = 0; j < dimension; j++)
		{
			if (i == j)
				nI[i * dimension + j] = diagonalValue;
			else
				nI[i * dimension + j] = 0;
		}
	}
	return nI;
}

double* spd_create_transpose(double* A, unsigned int dimension)
{
	unsigned int i, j;
	double* A_t;
	A_t = spd_create_uninit(dimension);

	for (i = 0; i < dimension; i++) //transponowanie macierzy
	{
		for (j = 0; j < dimension; j++)
			A_t[i * dimension + j] = A[j * dimension + i];
	}

	return A_t;
}

double spd_random_double(double fMin, double fMax)
{
	double f = (double)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

void spd_print_matrix(double* A, unsigned int dimension, int count)
{
	unsigned int i, j;
	if (dimension < count)
		count = dimension;

	for (i = 0; i < count; i++)
	{
		for (j = 0; j < count; j++)
		{
			printf("%0.2f\t", A[i * dimension + j]);
		}
		printf("\n");
	}
}


double* spd_make_random_matrix(double* A, unsigned int dimension, double minValue, double maxValue)
{
	unsigned int i, j;

	for (i = 0; i < dimension; i++)
	{
		for (j = 0; j < dimension; j++)
		{
			A[i * dimension + j] = spd_random_double(minValue, maxValue);
		}
	}
	return A;
}

double* spd_clone_matrix(double* A, unsigned int dimension)
{
	double* cloned_matrix = spd_create_uninit(dimension);
	unsigned int i, j;
	for (i = 0; i < dimension; i++)
	{
		for (j = 0; j < dimension; j++)
		{
			cloned_matrix[i * dimension + j] = A[i * dimension + j];
		}
	}
	return cloned_matrix;
}

double* spd_make_symetric(double* A, unsigned int dimension)
{
	unsigned int i, j;

	for (i = 0; i < dimension; i++) // A = A+A'
		for (j = 0; j < dimension; j++)
			A[i * dimension + j] = A[j * dimension + i];

	return A;
}

double* spd_make_positive_definite(double* A, unsigned int dimension, double offset)
{
	unsigned int i;

	for (i = 0; i < dimension; i++) // A = A + n*I(n);
		A[i * dimension + i] = A[i * dimension + i] + offset;

	return A;
}

int spd_comapre_matrices(double *A, double *B, int dimension, double epsilon) {
	int correct = 1;
	int errors = 0;
	int i, j;
	for (i = 0; i < dimension; i++) {
		for (j = 0; j < dimension; j++) {
			if (fabs(A[i * dimension + j] - B[i * dimension + j]) > epsilon) {
				if (correct)
					printf("  (%d, %d): %0.5f != %0.5f\n", i, j, A[i * dimension + j], B[i * dimension + j]);
				correct = 0;
				errors++;
			}
		}
	}
	printf("  Total errors: %d\n", errors);
	return errors;
}

void spd_free_matrix(double *A) {
	free(A);
}


// float
float *spd_create_uninitf(unsigned int dimension) 
{
	float *m = (float*) calloc(dimension * dimension, sizeof(float));
	return m;
}

float *spd_create_blankf(unsigned int dimension) 
{
	float *m = (float*) calloc(dimension * dimension, sizeof(float));
	unsigned int i;
	for (i = 0; i < dimension*dimension; i++)
		m[i] = 0;
	return m;
}

float *spd_create_randomf(unsigned int dimension, float minValue, float maxValue) 
{
	float *m = (float*) calloc(dimension * dimension, sizeof(float));
	spd_make_random_matrixf(m, dimension, minValue, maxValue);

	return m;
}

float* spd_create_symetricf(unsigned int dimension, float minValue, float maxValue) 
{
	float *m = (float*) calloc(dimension * dimension, sizeof(float));
	unsigned int i,j;
	for (i = 0; i < dimension; i++) {
		for (j = 0; j <= i; j++) {
			m[i * dimension + j] = spd_random_float(minValue, maxValue);
			m[j * dimension + i] = m[i * dimension + j];
		}
	}
	return m;
}

float* spd_create_identity_matrixf(unsigned int dimension, float diagonalValue)
{
	float* nI = spd_create_uninitf(dimension);
	unsigned int i, j;
	for (i = 0; i < dimension; i++)
	{
		for (j = 0; j < dimension; j++)
		{
			if (i == j)
				nI[i * dimension + j] = diagonalValue;
			else
				nI[i * dimension + j] = 0;
		}
	}
	return nI;
}

float* spd_create_transposef(float* A, unsigned int dimension)
{
	unsigned int i, j;
	float* A_t;
	A_t = spd_create_uninitf(dimension);

	for (i = 0; i < dimension; i++) //transponowanie macierzy
	{
		for (j = 0; j < dimension; j++)
			A_t[i * dimension + j] = A[j * dimension + i];
	}

	return A_t;
}

float spd_random_float(float fMin, float fMax)
{
	float f = (float)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

void spd_print_matrixf(float* A, unsigned int dimension, int count)
{
	unsigned int i, j;
	if (dimension < count)
		count = dimension;

	for (i = 0; i < count; i++)
	{
		for (j = 0; j < count; j++)
		{
			printf("%0.2f\t", A[i * dimension + j]);
		}
		printf("\n");
	}
}


float* spd_make_random_matrixf(float* A, unsigned int dimension, float minValue, float maxValue)
{
	unsigned int i, j;

	for (i = 0; i < dimension; i++)
	{
		for (j = 0; j < dimension; j++)
		{
			A[i * dimension + j] = spd_random_float(minValue, maxValue);
		}
	}
	return A;
}

float* spd_clone_matrixf(float* A, unsigned int dimension)
{
	float* cloned_matrix = spd_create_uninitf(dimension);
	unsigned int i, j;
	for (i = 0; i < dimension; i++)
	{
		for (j = 0; j < dimension; j++)
		{
			cloned_matrix[i * dimension + j] = A[i * dimension + j];
		}
	}
	return cloned_matrix;
}

float* spd_make_symetricf(float* A, unsigned int dimension)
{
	unsigned int i, j;

	for (i = 0; i < dimension; i++) // A = A+A'
		for (j = 0; j < dimension; j++)
			A[i * dimension + j] = A[j * dimension + i];

	return A;
}

float* spd_make_positive_definitef(float* A, unsigned int dimension, float offset)
{
	unsigned int i;

	for (i = 0; i < dimension; i++) // A = A + n*I(n);
		A[i * dimension + i] = A[i * dimension + i] + offset;

	return A;
}

int spd_comapre_matricesf(float *A, float *B, int dimension, float epsilon) {
	int correct = 1;
	int errors = 0;
	int i, j;
	for (i = 0; i < dimension; i++) {
		for (j = 0; j < dimension; j++) {
			if (fabs(A[i * dimension + j] - B[i * dimension + j]) > epsilon) {
				if (correct)
					printf("  (%d, %d): %0.5f != %0.5f\n", i, j, A[i * dimension + j], B[i * dimension + j]);
				correct = 0;
				errors++;
			}
		}
	}
	printf("  Total errors: %d\n", errors);
	return errors;
}

void spd_free_matrixf(float *A) {
	free(A);
}
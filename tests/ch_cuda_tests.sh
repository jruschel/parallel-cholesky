#chmod u+x <>.sh
DESIGN="ch_cuda_design.csv"
BIN="../CUDA/cholesky "
LOG="ch_results_cuda.txt"
LOG_T="ch_log.txt"

DEVICE_ID="0"

TEST_NAME="Cholesky GPU CUDA"

echo "Starting $TEST_NAME Tests At: $(date)" 
printf "Starting $TEST_NAME Tests At: $(date)\n" >> $LOG_T

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size algorithm tpb cutoff time plataform
do
	if [ "${algorithm//[\"]/}" != "4" ]
	then
		echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${algorithm//[\"]/}"," ${tpb//[\"]/}"," ${cutoff//[\"]/}
		$BIN ${size//[\"]/} ${algorithm//[\"]/} ${tpb//[\"]/} ${cutoff//[\"]/} $DEVICE_ID >> $LOG
	fi
done

echo "Done $TEST_NAME Tests At: $(date)"
printf "Done $TEST_NAME Tests At: $(date)\n" >> $LOG_T

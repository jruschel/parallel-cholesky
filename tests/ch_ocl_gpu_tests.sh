#chmod u+x <>.sh
DESIGN="ch_ocl_design.csv"
BIN="../bin/cholesky-ocl "
LOG="ch_results_ocl_gpu.txt"
LOG_T="ch_log.txt"

# this is machine-specific
PLATFORM_ID="1"
DEVICE_ID="0"

TEST_NAME="Cholesky GPU OpenCL"

echo "Starting $TEST_NAME Tests At: $(date)" 
printf "Starting $TEST_NAME Tests At: $(date)\n" >> $LOG_T

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size algorithm cutoff time plataform
do
	if [ $((${name//[\"]/} % 5)) == "0" ]
	then
		echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${algorithm//[\"]/}"," ${cutoff//[\"]/}
	fi
	$BIN ${size//[\"]/} ${algorithm//[\"]/} ${cutoff//[\"]/} $PLATFORM_ID $DEVICE_ID >> $LOG
done

echo "Done $TEST_NAME Tests At: $(date)"
printf "Done $TEST_NAME Tests At: $(date)\n" >> $LOG_T

#chmod u+x <>.sh
DESIGN="ch_seq_design.csv"
BIN="../bin/cholesky-seq "
LOG="ch_results_seq.txt"
LOG_T="ch_log.txt"

TEST_NAME="Cholesky SEQ"

echo "Starting $TEST_NAME Tests At: $(date)" 
printf "Starting $TEST_NAME Tests At: $(date)\n" >> $LOG_T

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size algorithm time plataform
do
	if [ $((${name//[\"]/} % 5)) == "0" ]
	then
		echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${algorithm//[\"]/}
	fi
	$BIN ${size//[\"]/} ${algorithm//[\"]/} >> $LOG
done

echo "Done $TEST_NAME Tests At: $(date)"
printf "Done $TEST_NAME Tests At: $(date)\n" >> $LOG_T

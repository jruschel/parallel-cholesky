#chmod u+x <>.sh
DESIGN="ch_omp_design.csv"
BIN="../bin/cholesky-omp "
LOG="ch_results_omp.txt"
LOG_T="ch_log.txt"

TEST_NAME="Cholesky CPU OpenMP"

echo "Starting $TEST_NAME Tests At: $(date)" 
printf "Starting $TEST_NAME Tests At: $(date)\n" >> $LOG_T

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size algorithm cutoff threads time plataform
do
	if [ $((${name//[\"]/} % 5)) == "0" ]
	then
		echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${algorithm//[\"]/}"," ${cutoff//[\"]/}"," ${threads//[\"]/}
	fi
	$BIN ${size//[\"]/} ${algorithm//[\"]/} ${cutoff//[\"]/} ${threads//[\"]/} >> $LOG
done

echo "Done $TEST_NAME Tests At: $(date)"
printf "Done $TEST_NAME Tests At: $(date)\n" >> $LOG_T

#chmod u+x <>.sh
DESIGN="ch_lapack_design.csv"
BIN="../bin/cholesky-lapack "
LOG="ch_results_lapack.txt"
LOG_T="ch_log.txt"

TEST_NAME="Cholesky CPU LAPACK"

echo "Starting $TEST_NAME Tests At: $(date)" 
printf "Starting $TEST_NAME Tests At: $(date)\n" >> $LOG_T

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size threads time plataform
do
	if [ $((${name//[\"]/} % 5)) == "0" ]
	then
		echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${threads//[\"]/}
	fi
	export OMP_NUM_THREADS=${threads//[\"]/}
	$BIN ${size//[\"]/} >> $LOG
done

echo "Done $TEST_NAME Tests At: $(date)"
printf "Done $TEST_NAME Tests At: $(date)\n" >> $LOG_T

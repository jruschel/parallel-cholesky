Parallel implementations of the Cholesky decomposition in CPU and GPU. 
	Author: João Paulo T Ruschel {jptruschel@inf.ufrgs.br}

This work is presented in partial fulfillment of the requirements for the degree of Bachelor in Computer Science at the Universidade Federal do Rio Grande do Sul (UFRGS), Brazil, 2016.

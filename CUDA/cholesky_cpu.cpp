/*
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/* Cholesky Decomposition CPU implementation for CUDA reference testing.
    by João Paulo T Ruschel (jptruschel@inf.ufrgs.br)

  Modified from template provided by NVIDIA.
*/

#include <math.h>

////////////////////////////////////////////////////////////////////////////////
// export C interface
extern "C"
void computeGold(float* A, float* L, const unsigned int dimensionSize);

////////////////////////////////////////////////////////////////////////////////
//! Compute reference data set
//! Each element is multiplied with the number of threads / array length
//! @param A              input data in global memory
//! @param L              output data in global memory
//! @param dimensionSize  width of matrices
////////////////////////////////////////////////////////////////////////////////
void 
computeGold(float* A, float* L, const unsigned int dimensionSize)
{
	int i, j, k;
	float sum;
  for (j = 0; j < dimensionSize; j++) {
  	sum = 0;
    for (k = 0; k < j; k++) {
      sum += L[j * dimensionSize + k] * L[j * dimensionSize + k];
    }
    L[j * dimensionSize + j] = sqrt(A[j * dimensionSize + j] - sum);

    for (i = j + 1; i < dimensionSize; i++) {
      sum = 0;
      for (k = 0; k < j; k++) {
        sum += L[i * dimensionSize + k] * L[j * dimensionSize + k];
      }
      L[i * dimensionSize + j] = (1.0 / L[j * dimensionSize + j] * 
        (A[i * dimensionSize + j] - sum));
    }
  }
}
